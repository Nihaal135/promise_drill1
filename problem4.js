/* Q4. Write a function that takes any number of arguments (userIds) ..and makes api call to fetch the user details.

        A. use Promises.all and make api call for each user
        B. use only 1 api call to get all the results.
 */
import fetch from 'node-fetch'

let inputArray = [1, 2, 3, 4, 5,6, 7, 8, 9, 10];

function apiCallForEachUser(inputArray, url) {
    Promise.all(inputArray.map((id) => {

        return fetch(url + "/" + id)
        .then((data) => data.json())
    }))
    .then((res) => { console.log(res);});
}

apiCallForEachUser(inputArray, 'https://jsonplaceholder.typicode.com/users')

function apiCallForResult(url) {
    fetch(url).then(response => response.json())
        .then((res) => {
            console.log(res);
        });
}

apiCallForResult('https://jsonplaceholder.typicode.com/users')