import fetch from 'node-fetch';
let resultObject = {};
// fetch('https://jsonplaceholder.typicode.com/users')
//   .then(response => response.json())
//   .then(data => console.log(data));

// fetch('https://jsonplaceholder.typicode.com/todos')
//   .then(response => response.json())
//   .then(data => console.log(data))

//Question 1 using fetch keyowrd
//Part 1 : Fetch data using promise
function fetchTodos(){
    fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(data =>console.log(data))
}
fetchTodos();

//Part 2 : Use await keyword
async function fetchTodosData(){
    let input = await fetch('https://jsonplaceholder.typicode.com/todos');
    let output = await input.json();
    return output;
}

//Part 3 : Once the list is fetched.Group the list of tasks based on user IDs.
// Make sure the non completed tasks are always in front.
// { 
//     userId1: [ // All the tasks of userId1]
//         ,
//     userId2: [ // All the tasks of userId2]


//     ...

fetchTodosData().then((data)=>{
    resultObject['B'] = data;
    
    let userIDList = data.reduce((gatherer, currentValue) => {
        if(gatherer[currentValue.userId]){ //gatherer[0];
            if(currentValue.completed){
                gatherer[currentValue.id].push(currentValue);
            } else {
                gatherer[currentValue.id].unshift(currentValue);
            }
        } else {
            gatherer[currentValue.userId] = [];
            if(currentValue.completed){
                gatherer[currentValue.id].push(currentValue);
            } else {
                gatherer[currentValue.id].unshift(currentValue);
            }
        }
        return gatherer;
    },[]);
    resultObject['C'] = userIDList;

    // D.  Also Group tasks based on completed or nonCompleted for each user
    // { 
    //     completed: [..All the completed tasks],
    //     nonCompleted: [...All the non completed tasks]
    //   }

     let completedRsult = data.reduce((gatherer, currentValue)=>{
        if(currentValue.completed){
            if(gatherer.completed){
                gatherer['completed'].push(currentValue);
            } else {
                gatherer['completed'] = [];
                gatherer['completed'].push(currentValue);
            }
        } else {
            if(gatherer['completed']){
                gatherer['nonCompleted'].push(currentValue);
            } else {
                gatherer['nonCompleted'] = [];
                gatherer['nonCompleted'].push(currentValue);
            }
        }
        return gatherer;
     }, []);
     resultObject['D'] = completedRsult;
});
console.log(resultObject);