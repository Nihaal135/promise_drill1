/* Q2. Write a method that returns a promise to read a File (use fs.readFile inside). 
        your method readFile should use fs.readFile and return a promise.
        your method readFile should accept a file path.
        your method should throw an error with message saying "Missing File." if path is incorrect.
        your method should also accept another optional params for transformation of the data .        
       
        write a method for Transformation of data after read operation ...in the form [solutionA, solutionB, solutionC, solutionD]. */

//const fs = require('fs');
import fs from 'fs'

function promiseRead(path, transform) {    
    return new Promise((resolve, reject)=>{
        if(!path)
        {
            reject("Path is not there")
        } else {
            fs.readFile(path,'utf8', (err, data)=>{
                if(err)
                {
                    console.log('Missing file');
                }
                 resolve(transform(data))
            });
        }
    });
}

function transformData(data) {   
    let parsedData = JSON.parse(data);
    let finalArray = [];
    for(let key in parsedData)
    {
        finalArray.push(parsedData[key])
    }
    return finalArray;
}

promiseRead('./content.txt', transformData).then((tranformedData)=>{
    console.log(tranformedData);
});
