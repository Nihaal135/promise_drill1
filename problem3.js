/* Q3. Write a function to get all the users information from the users API url. Find all the users with name "Nicholas". 
Get all the to-dos for those user ids. */
import fetch from "node-fetch"

fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => response.json())
    .then((data) => {
        let idList = data.reduce((gatherer, currentValue) => {
            if (currentValue.name.includes("Nicholas")) {
                gatherer.push(currentValue.id);
            }
            return gatherer;
        }, []);

        fetch('https://jsonplaceholder.typicode.com/todos')
            .then((response) => response.json())
            .then((data) => {
                let nicholasData = data.filter((individualElement) => {
                    if (Number(individualElement.id) == Number(idList[0])) {
                        return individualElement;
                    }
                })
                console.log(nicholasData);
            });
    });